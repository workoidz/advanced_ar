# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Person.destroy_all

Person.create!(
	[
		{first_name: "Ritesh",last_name: "kapoor",age: 36, login: "ritesh", pass: "star@123"},
		{first_name: "Ravi", last_name: "Sharma", age: 26, login: "ravi", pass: "starts"},
		{first_name: "Deepak", last_name: "Sharma", age: 31, login: "deepak", pass:"i am allow"},
		{first_name: "Rohit", last_name: "Gupta", age: 40, login: "rohit", pass: "weare"},
		{first_name: "kunal", last_name: "Bhardwaj", age: 29, login: "kunal", pass: "iamcoming"}
	]
	)